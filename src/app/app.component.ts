import { Component, ViewChild } from '@angular/core';
import { Validators } from '@angular/forms';
import { DynamicFormComponent } from './components/dynamic-form/dynamic-form.component';
import { ControlConfig } from './models/control.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  @ViewChild(DynamicFormComponent, { static: true }) form: DynamicFormComponent;
  definitions: ControlConfig[] = [
    {
      type: 'input',
      id: 'username',
      label: 'Username',
      inputType: 'text',
      name: 'username',
      value: 'initial value',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Name Required',
        },
        {
          name: 'pattern',
          validator: Validators.pattern('^[a-zA-Z]+$'),
          message: 'Accept only text',
        },
      ],
    },
    {
      type: 'input',
      id: 'email',
      label: 'Email Address',
      inputType: 'email',
      name: 'email',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Email Required',
        },
        {
          name: 'pattern',
          validator: Validators.pattern(
            '^[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$'
          ),
          message: 'Invalid email',
        },
      ],
    },
    {
      type: 'input',
      id: 'password',
      label: 'Password',
      inputType: 'password',
      name: 'password',
      validations: [
        {
          name: 'required',
          validator: Validators.required,
          message: 'Password Required',
        },
      ],
    },
    // {
    //   type: 'radiobutton',
    //   label: 'Gender',
    //   name: 'gender',
    //   options: [
    //     { value: '1', label: 'Male' },
    //     { value: '2', label: 'Female' },
    //   ],
    //   value: 'Male',
    // },
    // {
    //   type: 'date',
    //   label: 'DOB',
    //   name: 'dob',
    //   validations: [
    //     {
    //       name: 'required',
    //       validator: Validators.required,
    //       message: 'Date of Birth Required',
    //     },
    //   ],
    // },
    {
      type: 'selectcombobox',
      label: 'Country',
      name: 'country',
      value: '3',
      options: [
        { value: '1', label: 'India' },
        { value: '1', label: 'UAE' },
        { value: '3', label: 'UK' },
        { value: '4', label: 'US' },
      ],
    },
    {
      type: 'list',
      label: 'Example List',
      name: 'list',
      multipleSelection: false,
      options: [
        { value: '1', label: 'India', selected: true },
        { value: '1', label: 'UAE' },
        { value: '3', label: 'UK' },
        { value: '4', label: 'US' },
      ],
    },
    // {
    //   type: 'checkbox',
    //   label: 'Accept Terms',
    //   name: 'term',
    //   value: true,
    // },
    {
      type: 'button',
      label: 'Save',
    },
  ];

  submit(value: any) {}
}
