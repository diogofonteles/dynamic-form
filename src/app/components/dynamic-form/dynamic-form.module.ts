import { DynamicControlDirective } from './../../directives/dynamic-control.directive';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicFormComponent } from './dynamic-form.component';
import { InputComponent } from './components/input/input.component';
import { MatButtonModule, MatInputModule, MatListModule, MatSelectModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { SelectComboBoxComponent } from './components/select-combo-box/select-combo-box.component';
import { ButtonComponent } from './components/button/button.component';
import { ListComponent } from './components/list/list.component';



@NgModule({
  declarations: [DynamicFormComponent, InputComponent, DynamicControlDirective, SelectComboBoxComponent, ButtonComponent, ListComponent],
  imports: [
    CommonModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    ReactiveFormsModule
  ],
  exports: [DynamicFormComponent],
  entryComponents: [
    InputComponent,
    SelectComboBoxComponent,
    ButtonComponent,
    ListComponent
    // SelectComponent,
    // DateComponent,
    // RadiobuttonComponent,
    // CheckboxComponent
  ]
})
export class DynamicFormModule { }
