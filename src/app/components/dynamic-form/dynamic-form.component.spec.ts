import { ReactiveFormsModule, Validators } from '@angular/forms';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DynamicFormComponent } from './dynamic-form.component';

describe('DynamicFormComponent', () => {
  let component: DynamicFormComponent;
  let fixture: ComponentFixture<DynamicFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DynamicFormComponent],
      imports: [ReactiveFormsModule],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DynamicFormComponent);
    component = fixture.componentInstance;

    component.controls = [
      {
        type: 'input',
        id: 'username',
        label: 'Username',
        inputType: 'text',
        name: 'username',
        value: 'initial value',
        validations: [
          {
            name: 'required',
            validator: Validators.required,
            message: 'Name Required',
          },
          {
            name: 'pattern',
            validator: Validators.pattern('^[a-zA-Z]+$'),
            message: 'Accept only text',
          },
        ],
      },
      {
        type: 'input',
        id: 'email',
        label: 'Email Address',
        inputType: 'email',
        name: 'email',
        validations: [],
      },
    ];

    component.ngOnInit();

    fixture.detectChanges();
  });

  it('should render input elements', () => {
    const compiled = fixture.debugElement.nativeElement;
    const userNameInput = compiled.querySelector('input[id="username"]');
    const emailInput = compiled.querySelector('input[id="email"]');

    expect(userNameInput).toBeTruthy();
    expect(emailInput).toBeTruthy();
  });

  it('should test form validity', () => {
    const form = component.form;
    expect(form.valid).toBeFalsy();

    const userNameInput = form.controls.username;
    userNameInput.setValue('John Peter');

    expect(form.valid).toBeTruthy();
  });

  it('should test input validity', () => {
    const userNameInput = component.form.controls.username;
    const emailInput = component.form.controls.email;

    expect(userNameInput.valid).toBeFalsy();
    expect(emailInput.valid).toBeTruthy();

    userNameInput.setValue('John Peter');
    expect(userNameInput.valid).toBeTruthy();
  });

  it('should test input errors', () => {
    const userNameInput = component.form.controls.username;
    expect(userNameInput.errors.required).toBeTruthy();

    userNameInput.setValue('John Peter');
    expect(userNameInput.errors).toBeNull();
  });

});
